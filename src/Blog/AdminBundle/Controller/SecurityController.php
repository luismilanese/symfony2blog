<?php

namespace Blog\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;


/**
 * Class SecurityController
 */
class SecurityController extends Controller
{
	/**
	 * Login
	 *
	 * @return Response
	 *
	 * @Route("/login")
	 */
	public function loginAction()
	{
		/** @var AuthenticationUtils $helper */
		$helper = $this->get('security.authentication_utils');

		return $this->render(
			'AdminBundle:Security:login.html.twig',
			[
				'last_username' => $helper->getLastUsername(),
				'error'         => $helper->getLastAuthenticationError()
			]
		);
	}

	/**
	 * Login check
	 *
	 * @Route("login_check")
	 * @Method("POST")
	 */
	public function loginCheckAction()
	{}

	/**
	 * Logout
	 *
	 * @Route("logout")
	 */
	public function logoutAction()
	{}
}