<?php

namespace Blog\ModelBundle\DataFixtures\ORM;

use Blog\ModelBundle\Entity\Author;
use Blog\ModelBundle\Entity\Post;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Fixtures for the post entity
 */
class Posts extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 15;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $p1 = new Post();
        $p1->setTitle('Lorem ipsum dolor sit amet');
        $p1->setBody('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ullamcorper, odio non aliquet facilisis, justo enim varius sem, eu varius ex leo non nibh. Integer eu erat quis dolor gravida porttitor. Aliquam eget metus nec lectus laoreet accumsan ac eu ex. Quisque rhoncus metus ut urna condimentum maximus. Nam quis enim a quam molestie pulvinar eu in felis. Nulla metus risus, imperdiet a nunc vel, faucibus dictum metus. Praesent dictum sodales eros, vitae dapibus sem fringilla eu. Mauris luctus purus eu justo commodo luctus.');
        $p1->setAuthor($this->getAuthor($manager, 'David'));

        $p2 = new Post();
        $p2->setTitle('Integer sit amet elit nec est');
        $p2->setBody('Integer sit amet elit nec est iaculis pretium id eu est. Donec sit amet neque commodo, imperdiet justo a, feugiat augue. Quisque suscipit sagittis tristique. Ut accumsan purus sed efficitur scelerisque. In ut luctus nisl. Integer convallis et dolor et placerat. Vestibulum in quam venenatis, pulvinar ipsum et, placerat massa.');
        $p2->setAuthor($this->getAuthor($manager, 'Elsa'));

        $p3 = new Post();
        $p3->setTitle('Quisque suscipit');
        $p3->setBody('Quisque suscipit sagittis tristique. Ut accumsan purus sed efficitur scelerisque. In ut luctus nisl. Integer convallis et dolor et placerat. Vestibulum in quam venenatis, pulvinar ipsum et, placerat massa.');
        $p3->setAuthor($this->getAuthor($manager, 'Elsa'));

        $manager->persist($p1);
        $manager->persist($p2);
        $manager->persist($p3);

        $manager->flush();
    }

    /**
     * Get an author
     * @param  ObjectManager $manager
     * @param  string        $name
     * 
     * @return Author
     */
    private function getAuthor(ObjectManager $manager, $name)
    {
        return $manager->getRepository('ModelBundle:Author')->findOneBy(
            ['name' => $name]
        );
    }
}