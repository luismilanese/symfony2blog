<?php

namespace Blog\ModelBundle\DataFixtures\ORM;

use Blog\ModelBundle\Entity\Comment;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Fixtures for the Comment entity
 */
class Comments extends AbstractFixture implements  OrderedFixtureInterface
{
	/**
	 * {@inheritDoc}
	 */
	public function getOrder()
	{
		return 20;
	}

	/**
	 * {@inheritDoc}
	 */
	public function load(ObjectManager $manager)
	{
		$posts = $manager->getRepository('ModelBundle:Post')->findAll();

		$comments = [
			0 => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi non pellentesque quam. Aenean sollicitudin euismod commodo. Sed non sagittis purus, ut consectetur nunc.',
			1 => 'Etiam ultricies eros sit amet mauris pharetra tincidunt. Proin et commodo ligula, in tincidunt felis. Vivamus orci leo, dictum luctus justo vel, varius pharetra tortor.',
			2 => 'Duis quis libero non lorem accumsan ornare. Vestibulum eleifend, nulla eu laoreet vehicula, sem tellus venenatis ante, a tincidunt nulla magna eget lectus.'
		];

		$i = 0;

		foreach($posts as $post) {
			$comment = new Comment();
			$comment->setAuthorName('Someone');
			$comment->setBody($comments[$i++]);
			$comment->setPost($post);

			$manager->persist($comment);
		}

		$manager->flush();
	}

}