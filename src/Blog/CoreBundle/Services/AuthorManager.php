<?php

namespace Blog\CoreBundle\Services;

use Blog\ModelBundle\Entity\Author;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class AuthorManager
 */
class AuthorManager
{
    private $em;

    /**
     * AuthorManager constructor.
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Find author by slug
     *
     * @param  string $slug
     * @throws NotFoundHttpException
     * @return Author
     */
    public function findBySlug($slug)
    {
        $author = $this->em->getRepository('ModelBundle:Author')->findOneBy(['slug' => $slug]);

        if ($author === null) {
            throw new NotFoundHttpException('Author was not found.');
        }

        return $author;
    }

    /**
     * Find all posts for a given author
     *
     * @param  Author $author
     * @return array
     */
    public function findPosts(Author $author)
    {
        return $this->em->getRepository('ModelBundle:Post')->findBy(['author' => $author]);
    }
}