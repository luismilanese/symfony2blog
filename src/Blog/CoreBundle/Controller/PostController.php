<?php

namespace Blog\CoreBundle\Controller;

use Blog\CoreBundle\Services\PostManager;
use Blog\ModelBundle\Entity\Comment;
use Blog\ModelBundle\Entity\Post;
use Blog\ModelBundle\Form\CommentType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class PostController
 *
 * @Route("/{_locale}", requirements={"_locale"="en|es"}, defaults={"_locale"="en"})
 */
class PostController extends Controller
{
    /**
     * Show the posts index
     *
     * @return array
     * 
     * @Route("/")
     * @Template()
     */
    public function indexAction()
    {
	    $posts = $this->getPostManager()->findAll();
		$latestPosts = $this->getPostManager()->findLatest(5);

        return [
	        'posts'       => $posts,
	        'latestPosts' => $latestPosts
        ];
    }

	/**
	 * Show a post
	 *
	 * @param string $slug
	 * @return array
	 *
	 * @throws NotFoundHttpException
	 *
	 * @Route("/{slug}")
	 * @Template()
	 */
	public function showAction($slug)
	{
		$post = $this->getPostManager()->findBySlug($slug);

		$form = $this->createForm(new CommentType());

		return [
			'post' => $post,
			'form' => $form->createView()
		];
	}

	/**
	 * Create comment
	 *
	 * @param Request $request
	 * @param string  $slug
	 *
	 * @return array
	 *
	 * @Route("/{slug}/create-comment")
	 * @Method("POST")
	 * @Template("CoreBundle:Post:show.html.twig")
	 */
	public function createCommentAction(Request $request, $slug)
	{
		/** @var Post $post */
		$post = $this->getPostManager()->findBySlug($slug);
		$form = $this->getPostManager()->createComment($post, $request);

		if ($form === true) {
			$this->get('session')->getFlashBag()->add('success', 'Your comment was submitted successfully');

			return $this->redirect($this->generateUrl('blog_core_post_show', ['slug' => $post->getSlug()]));
		}

		return [
			'post' => $post,
			'form' => $form->createView()
		];
	}

	/**
	 *Get post manager
	 *
	 * @return PostManager
	 */
	private function getPostManager()
	{
		return $this->get('postManager');
	}

}